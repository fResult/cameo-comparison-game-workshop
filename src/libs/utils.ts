export function pickRandom<T>(items: Array<T>): T {
  const randomIdx: number = Math.floor(items.length * Math.random())
  return items[randomIdx]
}

export function sleep(ms: number) {
  return new Promise((fulfil) => {
    setTimeout(fulfil, ms)
  })
}

export function loadImage(src: string) {
  return new Promise((fulfil, reject) => {
    const img = new Image()
    img.onload = () => fulfil(img)
    img.onerror = reject
    img.src = src
  })
}
