import type { Selection } from './../types/selection';
import type { Celeb } from '../types/celeb'

import { pickRandom } from './utils'

const ROUNDS_PER_GAME: number = 3 /*10*/

function remove<T>(items: Array<T>, index: number): void {
  // if a 'similar' account was picked, there's no
  // guarantee that it's in the filtered array
  if (index === -1) return

  // this is much faster than splicing the array
  items[index] = items[items.length - 1]
  items.pop()
}

export function select(
  celebs: Array<Celeb>,
  lookup: Map<string, Celeb>,
  category: string
) {
  const filtered: Array<Celeb> = celebs.filter((c, i) => {
    return (c.categories.includes(category as any)
  )})

  const seen = new Set<string>()
  const selection: Selection = []

  let i = ROUNDS_PER_GAME
  while (i--) {
    const n = Math.random()
    const ai = Math.floor(n * filtered.length)
    const a: Celeb = filtered[ai];

    // remove a from the array so this person can't be picked again
    remove(filtered, ai)

    let b: Celeb

    // if this celeb has 'similar' celebs, decide whether to pick one
    const similar = a.similar.filter((id) => !seen.has(id))
    if (similar.length > 0 && Math.random() < 0.75) {
      const id: string = pickRandom(similar)
      b = lookup.get(id)
    }

    // otherwise pick someone at random
    else {
      b = pickRandom(filtered)
    }

    selection.push({ a, b })

    seen.add(a.id)
    seen.add(b.id)
    console.log('seen', seen)

    // remove b from the array so this person can't be picked again
    remove(filtered, filtered.indexOf(b))
  }

  return selection
}
