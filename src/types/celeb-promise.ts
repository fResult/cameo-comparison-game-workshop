import type { Celeb } from "./celeb";

export type CelebsPromise = Promise<{
  celebs: Array<Celeb>
  lookup: Map<string, Celeb>
}>
