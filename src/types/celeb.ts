import type { Category } from "./category";

export type Celeb = {
  id: string
  name: string
  image: string
  rating: number
  price: number
  reviews: number
  categories: Array<Category>
  type: string,
  similar: Array<string>
}