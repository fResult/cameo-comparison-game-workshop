import type { Celeb } from "./celeb";

export type Selection = Array<{ a: Celeb, b: Celeb }>;
