export type Category = {
  slug: string
  label: string
}
